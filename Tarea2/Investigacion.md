strcpy:
Es para asgianar la expresion de cadena a un array de caracteres,
la funcion strcpy copa el contenido de la <cadena_fuete> en la <variable_destino>,
siendo esta un array de caracteres. para poder utilizar dicha funcion en un programa
hay que escribir: #include<string.h>

strtok:
Se usa para un proceso conocido como tokenization, que es el proceso de dividir una cadena de
texto en palabras, frases, simbolos u otro grupo de elementos, cada elemento recibe el nombre de token.
para poder utilizar dicha funcion en un programa hay que escribir: #include<string.h>
Recibe un puntero al primer caracter de la cadena a tokenizar y un puntero al primer caracter de la cadena a usar como delimitador:

char* strtok( char* cadena_a_tokenizar, const char* delimitador );

Strtok_s:
Es la versión segura de strtok que añade parámetros que facilitan el control y ayudan a detectar o evitar errores.

char *strtok_s(char *restrict cadena_a_tokenizar, rsize_t *restrict maximo,
	const char *restrict delimitador, char **restrict estado);
		
		
Los parametros de cadena_a_tokenizar y delimitador funcionan igual que la versión de C++
El parametro maximo debe apuntar a una variable que contenga la longitud de cadena_a_tokenizar,
la función strtok_s irá modificando dicha variable con los caracteres que quedan por examinar.
El parametro estado es un puntero a un caracter en que la función strtok_s irá guardando su estado interno.
Estas mejoras hacen que strtok_s sea más segura y hace posible usarla en contextos de concurrencia.

