#include <stdio.h>

	//Funcion para determinal el numero factorial de una entrada
	// la entrada debe ser un numero entero, numero es la entrada del usuario
	// contador finaliza el bucle del for y res almacena y realiza las operaciones matematicas
	
int main(){
	
	int numero = 0;
	int cont = 0;
	int res = 1;
	
	printf("Ingrese un numero: ");
	scanf(" %d", &numero);
	
	for(cont=1; cont<=numero; cont++){
		res = res*cont;
		
	}
	printf("El factorial de %d es %d \n", numero, res );
	
	return 0;
}
