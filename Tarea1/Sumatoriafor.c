#include <stdio.h>

	//Funcion para realiar una sumatoria mediante un for desde 0 hasta n numero  que se representa con variable numero
	// valor de entrada es entero, cont termina el bucle del for, res opera y guarda el resultado final 
	// detalleres solo es para guardar el numero ingresado sin modificarlo y mostrarlo en la solucion final

int main(){
	
	int numero = 0;
	int cont = 0;
	int res = 0;
	int detalleres = 0;
	
	printf("Ingrese el numero a realizar la sumatoria: ");
	scanf(" %d", &numero);
	detalleres = numero;

	for(cont=1; cont<=numero; cont++){
		res = res+cont;
	}
	
	printf("La sumatiria de %d es: %d \n", detalleres, res);
	
	return 0;
}
