#include<stdio.h>
	//Funcion para verificar si un numero es par o impar
	//retorna la indicacion si el numero es par o impar
	//el numero a ingresar debe ser entero
int main(){
	
	int numero;
	
	printf("Ingrese el numero: ");
	scanf("%d", &numero);
	
	if(numero%2==0){
		printf("%d es un numero par \n", numero);
	}
	else{
		printf("%d es un numero impar \n", numero);
		
	}
	return 0;
}
