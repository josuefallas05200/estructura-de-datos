#include<stdio.h>

int main(){
	//Funcion para verificar si un numero es par o impar
	//retorna la indicacion si el numero es par o impar
	//el numero a ingresar debe ser entero
	int numero;
	
	printf("Ingrese el numero: ");
	scanf("%d", &numero);
	
	if(numero%2==0){
		printf("%d es un numero par \n", numero);
	}
	else{
		printf("%d es un numero impar \n", numero);
		
	}
	return 0;
}
//------------------------------------------------------------------------------------------------------
	//Funcion realizar el bucle fibonacci hasta la cantidad repeticiones que el usuario desee
	//la cantidad de repeticiones que desee el usuario debe de ser una cantidad de numeros enteros
int main(){
	
	// primernumero y segundomunero, variables de inicio del fibonacci que inica en 0 y 1
	// elementos para saber cuantas veces repetir el bucle
	// temporal para imprimir el valor de la funcion y cambiar valores de primer y segundo numero
	// i ayuda a terminar el ciclo
	
	int elementos = 0;
	int primernumero = 0;
	int segundonumero = 1;
	int temp = 0;
	int i = 0;
	
	printf("Ingrese de elementos: ");
	scanf(" %d", &elementos);
	
	for(i=1; i<=elementos; i++){
		temp = primernumero + segundonumero;
		primernumero = segundonumero;
		segundonumero = temp;
		
		printf(" %d, ",temp);
	}
	
	
	return 0;
}
//----------------------------------------------------------------------------------------------------------

	//Funcion para determinal el numero factorial de una entrada
	// la entrada debe ser un numero entero, numero es la entrada del usuario
	// contador finaliza el bucle del for y res almacena y realiza las operaciones matematicas
	
int main(){
	
	int numero = 0;
	int cont = 0;
	int res = 1;
	
	printf("Ingrese un numero: ");
	scanf(" %d", &numero);
	
	for(cont=1; cont<=numero; cont++){
		res = res*cont;
		
	}
	printf("El factorial de %d es %d \n", numero, res );
	
	return 0;
}

//------------------------------------------------------------------------------------------------------------

	//Funcion para determinar el largo de un numero mediante un for
	// entrada un numero entero, largo determina el largo del numero
	// detalleres es solo para mostrar el digito ingresado por el usuario al dar una respuesta
	//contador termina el bucle del for
	
int main(){
	
	int numero = 0;
	int largo = 0;
	int cont = 0;
	int detalleres = 0;
	
	printf("Ingrese un numero: ");
	scanf(" %d", &numero);
	detalleres = numero;
	
	for(cont=0; cont<numero;){
		largo++;
		numero = numero/10;
	}
	printf("El lago del numero %d es: %d \n", detalleres, largo);
	
	return 0;
}
//--------------------------------------------------------------------------------------------------------------

	//Funcion para determinar el largo de un numero mediante un for
	// entrada un numero entero, largo determina el largo del numero
	// detalleres es solo para mostrar el digito ingresado por el usuario al dar una respuesta
	//contador termina el bucle del while
	
int main(){
	
	int numero = 0;
	int largo = 0;
	int cont = 0;
	int detalleres = 0;
	
	printf("Ingrese un numero: ");
	scanf(" %d", &numero);
	detalleres = numero;
	
	while(cont<numero){
		largo++;
		numero = numero/10;
	}
	
	printf("El largo de %d es: %d \n", detalleres, largo);
	
	return 0;
}
//-------------------------------------------------------------------------------------------------------------------

	//Funcion para realiar una sumatoria mediante un for desde 0 hasta n numero  que se representa con variable numero
	// valor de entrada es entero, cont termina el bucle del for, res opera y guarda el resultado final 
	// detalleres solo es para guardar el numero ingresado sin modificarlo y mostrarlo en la solucion final

int main(){
	
	int numero = 0;
	int cont = 0;
	int res = 0;
	int detalleres = 0;
	
	printf("Ingrese el numero a realizar la sumatoria: ");
	scanf(" %d", &numero);
	detalleres = numero;

	for(cont=1; cont<=numero; cont++){
		res = res+cont;
	}
	
	printf("La sumatiria de %d es: %d \n", detalleres, res);
	
	return 0;
}

//--------------------------------------------------------------------------------------------------------------------------

	//Funcion para realiar una sumatoria mediante un while desde 0 hasta n numero  que se representa con variable numero
	// valor de entrada es entero, cont termina el bucle del while, res opera y guarda el resultado final 
	// detalleres solo es para guardar el numero ingresado sin modificarlo y mostrarlo en la solucion final
int main(){
	
	int numero = 0;
	int cont = 0;
	int res = 0;
	int detalleres = 0;
	
	printf("Ingrese el numero a realizar la sumatoria: ");
	scanf(" %d", &numero);
	detalleres = numero;

	while(cont<numero){
		cont++;
		res = res+cont;
		
	}
	printf("La sumatoria del numero %d es : %d \n", detalleres, res);
	
	return 0;
}
//-----------------------------------------------------------------------------------------------------------------------------

	// Funcion para calcular el inverso de un numero ingresado
	// El numero ingresado debe ser entero, variable inverso realiza la opeacion y contiene el numero inverso
	//res obtiene el ultimo digito del numero ingresado para luego operarlo en inverso
	// detalleres solo es para guardar el numero ingresado sin modificarlo y mostrarlo en la solucion final
int main(){
	
	int numero = 0;
	int inverso = 0;
	int res = 0;
	int detalleres = 0;
	
	printf("Ingrese el numero a realizar el inverso: ");
	scanf(" %d", &numero);
	detalleres = numero;
	
	while(numero!=0){
		res = numero%10;
		numero = numero/10;
		inverso = inverso*10+res;
	}
	
	printf("El inverso de %d es: %d \n", detalleres, inverso);
	return 0;
}
//------------------------------------------------------------------------------------------------------------------------------

	//Funcion para determinar si un numero es palindromo, la entrada debe de ser un numero entero
	//Funcion calula el inverso del nuero y luego lo compara para determinar si es palindromo
	//numnormal guarda el numero dado por el usuario y lo conserva para luego compararlo con su inverso
int main(){
	
	int numero = 0;
	int inverso = 0;
	int res = 0;
	int numnormal = 0;
	
	printf("Ingrese el numero a realizar el inverso: ");
	scanf(" %d", &numero);
	numnormal = numero;
	
	while(numero!=0){
		res = numero%10;
		numero = numero/10;
		inverso = inverso*10+res;
	}
	
	if(inverso==numnormal){
		printf("El numero %d es palindromo ya que su inverso es: %d \n", numnormal, inverso);
	}
	else{
		printf("El numero %d no es palindromo ya que su inverso es: %d \n", numnormal, inverso);
	}
	return 0;
	//NOTA: reutilice el codigo anteriormente empleado para realizar el inverso
	//utilziando makefile me hubiese ahorrado esas lineas de codigo extra xD
}
