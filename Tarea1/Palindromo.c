#include <stdio.h>

	//Funcion para determinar si un numero es palindromo, la entrada debe de ser un numero entero
	//Funcion calula el inverso del nuero y luego lo compara para determinar si es palindromo
	//numnormal guarda el numero dado por el usuario y lo conserva para luego compararlo con su inverso
int main(){
	
	int numero = 0;
	int inverso = 0;
	int res = 0;
	int numnormal = 0;
	
	printf("Ingrese el numero a realizar el inverso: ");
	scanf(" %d", &numero);
	numnormal = numero;
	
	while(numero!=0){
		res = numero%10;
		numero = numero/10;
		inverso = inverso*10+res;
	}
	
	if(inverso==numnormal){
		printf("El numero %d es palindromo ya que su inverso es: %d \n", numnormal, inverso);
	}
	else{
		printf("El numero %d no es palindromo ya que su inverso es: %d \n", numnormal, inverso);
	}
	return 0;
	//NOTA: reutilice el codigo anteriormente empleado para realizar el inverso
	//utilziando makefile me hubiese ahorrado esas lineas de codigo extra xD
}
