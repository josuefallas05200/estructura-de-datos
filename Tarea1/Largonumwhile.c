#include <stdio.h>

	//Funcion para determinar el largo de un numero mediante un for
	// entrada un numero entero, largo determina el largo del numero
	// detalleres es solo para mostrar el digito ingresado por el usuario al dar una respuesta
	//contador termina el bucle del while
	
int main(){
	
	int numero = 0;
	int largo = 0;
	int cont = 0;
	int detalleres = 0;
	
	printf("Ingrese un numero: ");
	scanf(" %d", &numero);
	detalleres = numero;
	
	while(cont<numero){
		largo++;
		numero = numero/10;
	}
	
	printf("El largo de %d es: %d \n", detalleres, largo);
	
	return 0;
}
