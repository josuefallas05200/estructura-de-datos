#include <stdio.h>

	//Funcion realizar el bucle fibonacci hasta la cantidad repeticiones que el usuario desee
	//la cantidad de repeticiones que desee el usuario debe de ser una cantidad de numeros enteros
int main(){
	
	// primernumero y segundomunero, variables de inicio del fibonacci que inica en 0 y 1
	// elementos para saber cuantas veces repetir el bucle
	// temporal para imprimir el valor de la funcion y cambiar valores de primer y segundo numero
	// i ayuda a terminar el ciclo
	
	int elementos = 0;
	int primernumero = 0;
	int segundonumero = 1;
	int temp = 0;
	int i = 0;
	
	printf("Ingrese de elementos: ");
	scanf(" %d", &elementos);
	
	for(i=1; i<=elementos; i++){
		temp = primernumero + segundonumero;
		primernumero = segundonumero;
		segundonumero = temp;
		
		printf(" %d, ",temp);
	}
	
	
	return 0;
}
	
