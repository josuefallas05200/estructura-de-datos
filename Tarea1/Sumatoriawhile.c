#include <stdio.h>

	//Funcion para realiar una sumatoria mediante un while desde 0 hasta n numero  que se representa con variable numero
	// valor de entrada es entero, cont termina el bucle del while, res opera y guarda el resultado final 
	// detalleres solo es para guardar el numero ingresado sin modificarlo y mostrarlo en la solucion final
int main(){
	
	int numero = 0;
	int cont = 0;
	int res = 0;
	int detalleres = 0;
	
	printf("Ingrese el numero a realizar la sumatoria: ");
	scanf(" %d", &numero);
	detalleres = numero;

	while(cont<numero){
		cont++;
		res = res+cont;
		
	}
	printf("La sumatoria del numero %d es : %d \n", detalleres, res);
	
	return 0;
}
