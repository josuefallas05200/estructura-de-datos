#include<stdio.h>
#include<stdlib.h>

	//Funcion que agarra un texto y elimina el los caracteres 'C' y 'c'
	//La entrada es un arreglo de caracteres que forman un string
	//segun la declaracion del puntero, el espacio solicitado para el heap va a ser de 20 para la variable palabra

int main(){
	
	int i=0;
	char* palabra = calloc(20, sizeof(char));
	
	printf("Ingrese su palabra: ");
	scanf("%s", palabra);
	
	while(palabra[i]!='\0'){
		
		if(palabra[i]=='C'){
		i++;
		}
		
		else if(palabra[i]=='c'){
		i++;
		}
		else{
			printf("%c",palabra[i]);
			i++;
		}
	}
	free(palabra);
	return 0;
}
