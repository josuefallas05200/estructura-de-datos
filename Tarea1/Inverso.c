#include <stdio.h>
	// Funcion para calcular el inverso de un numero ingresado
	// El numero ingresado debe ser entero, variable inverso realiza la opeacion y contiene el numero inverso
	//res obtiene el ultimo digito del numero ingresado para luego operarlo en inverso
	// detalleres solo es para guardar el numero ingresado sin modificarlo y mostrarlo en la solucion final
int main(){
	
	int numero = 0;
	int inverso = 0;
	int res = 0;
	int detalleres = 0;
	
	printf("Ingrese el numero a realizar el inverso: ");
	scanf(" %d", &numero);
	detalleres = numero;
	
	while(numero!=0){
		res = numero%10;
		numero = numero/10;
		inverso = inverso*10+res;
	}
	
	printf("El inverso de %d es: %d \n", detalleres, inverso);
	return 0;
}
